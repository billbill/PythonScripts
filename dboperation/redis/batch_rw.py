# -*- coding=utf-8 -*-
import redis
import datetime

#inf = open("./result","r")

class Database:
    def __init__(self,host):
        self.host = host
        self.port = 6379
        self.write_pool = {}

    def write(self,tradedate,tradetime,setno,recno,buyorderrecno,sellorderrecno,securityid,price,tradeqty,pricemultradeqty,bs,value):
        try:
            key = '_'.join([str(tradedate),str(tradetime),str(securityid),str(price),str(tradeqty)])
            val = value
            r = redis.StrictRedis(host=self.host,port=self.port)
            r.set(key,val)
        except Exception, exception:
            print exception

    def read(self,tradedate,tradetime,setno,recno,buyorderrecno,sellorderrecno,securityid,price,tradeqty,pricemultradeqty,bs):
        try:
            key = '_'.join([str(tradedate),str(tradetime),str(securityid),str(price),str(tradeqty)])
            r = redis.StrictRedis(host=self.host,port=self.port)
            value = r.get(key)
            print value
            return value
        except Exception, exception:
            print exception

    def add_write(self,tradedate,tradetime,setno,recno,buyorderrecno,sellorderrecno,securityid,price,tradeqty,pricemultradeqty,bs,value):
        key = '_'.join([str(tradedate),str(tradetime),str(securityid),str(price),str(tradeqty)])
        val = value
        self.write_pool[key] = val

    def batch_write(self):
        try:
            r = redis.StrictRedis(host=self.host,port=self.port)
            r.mset(self.write_pool)
        except Exception, exception:
            print exception

def add_data():
    inf = open("./result", "r")
    beg = datetime.datetime.now()
    #redisdb = Database('172.18.1.67')
    redisdb = Database('localhost')
    BATCH_NUM = 2000
    i = 0
    while True:
        i = i + 1
        line = inf.readline()
        if not line:
            redisdb.batch_write()
            break
        line_tuple = str(line).split()
        redisdb.add_write(line_tuple[0],line_tuple[1],line_tuple[2],line_tuple[3],line_tuple[4],line_tuple[5],line_tuple[6],line_tuple[7],line_tuple[8],line_tuple[9],line_tuple[10],line.strip())
        if i == BATCH_NUM:
            redisdb.batch_write()
            i = 0
    end = datetime.datetime.now()
    print end-beg
    inf.close

def get_data():
    inf = open("./result", "r")
    beg = datetime.datetime.now()
    #redisdb = Database('172.18.1.67')
    redisdb = Database('localhost')
    while True:
        line = inf.readline()
        if not line:
            break
        line_tuple = str(line).split()
        redisdb.read(line_tuple[0],line_tuple[1],line_tuple[2],line_tuple[3],line_tuple[4],line_tuple[5],line_tuple[6],line_tuple[7],line_tuple[8],line_tuple[9],line_tuple[10])
    end = datetime.datetime.now()
    print end-beg
    inf.close

if __name__ == '__main__':
    add_data()
    #get_data()
