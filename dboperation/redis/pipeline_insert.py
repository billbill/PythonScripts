# -*- coding=utf-8 -*-

import redis
import datetime
import os

def gen_redis_proto_file():
    beg = datetime.datetime.now()
    try:
        inf = open("./result", "r")
        ouf = open("./pipeline_file", "w")
        while True:
            line = inf.readline()
            if not line:
                break
            (tradedate,tradetime,setno,recno,buyorderrecno,sellorderrecno,securityid,price,tradeqty,pricemultradeqty,bs) = str(line).split()
            key = '-'.join([str(tradedate),str(tradetime),str(setno),str(recno),str(buyorderrecno),str(sellorderrecno),str(securityid),str(price),str(tradeqty),str(pricemultradeqty),str(bs)])
            val = line.strip()
            pipeline_line = " SET "+key+" '"+str(val)+"'\n"
            ouf.write(pipeline_line)
        ouf.close()
        inf.close()
    except Exception, exception:
        print exception
    end = datetime.datetime.now()
    print "Time token by generating redis protocal file: "
    print end-beg

if __name__ == "__main__":
    gen_redis_proto_file()
    beg = datetime.datetime.now()
    os.system("cat ./pipeline_file | redis-cli --pipe")
    end = datetime.datetime.now()
    print "Time token by mass-inserting to redis: "
    print end-beg
