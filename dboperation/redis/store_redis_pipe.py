#-*- coding=utf-8 -*-

import redis
import re
import datetime
import sys

class Database():
    def __init__(self,host,port):
        self.host = host
        self.port = port

    # use a list to store 1 days trade data of one security
    def store_redis_pipe(self):
        try:
            inf = open("../cases/pre_result", "r")
            count = 0
            batch_size = 1000
            r = redis.StrictRedis(host=self.host,port=self.port)
            pipe = r.pipeline()
            while True:
                line = inf.readline()
                if not line:
                    pipe.execute()
                    break
                count = count+1
                match = re.search(r'(2015\d{4}.*)', line.strip())
                invalid = re.search(r'(2015\d{4}.*\[.*)', line.strip()) # temporary way, fix me
                if match and not invalid:
                    (tradedate,tradetime,setno,recno,buyorderrecno,sellorderrecno,securityid,price,tradeqty,pricemultradeqty,bs) = str(line).split()
                    #key = '-'.join([str(securityid),str(tradedate)])
                    key = str(securityid)
                    val = str(line.strip())
                    pipe.rpush(key,str(val))
                    if not count % batch_size:
                        pipe.execute()
                        count = 0
            inf.close
        except Exception, exception:
            print exception

if __name__ == "__main__":
    #redisdb = Database("172.18.1.67","6379")
    redisdb = Database(str(sys.argv[1]),str(sys.argv[2]))
    beg = datetime.datetime.now()
    redisdb.store_redis_pipe()
    end = datetime.datetime.now()
    print "Time token by mass-inserting to redis: "
    print end-beg
