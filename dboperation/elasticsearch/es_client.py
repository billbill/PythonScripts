# -*- coding=utf-8 -*-
import pyes

conn = pyes.ES(['localhost:9200'])#连接es

def creat():
    conn.indices.create_index('test-index')

    ##定义索引存储结构
    #mapping = { u'id': {'boost': 1.0,
    #                      'index': 'not_analyzed',
    #                      'store': 'yes',
    #                      'type': u'string',
    #                      "term_vector" : "with_positions_offsets"},
    #              u'user_id': {'boost': 1.0,
    #                         'index': 'not_analyzed',
    #                         'store': 'yes',
    #                         'type': u'string',
    #                         "term_vector" : "with_positions_offsets"},
    #              u'nick': {'boost': 1.0,
    #                         'index': 'analyzed',
    #                         'store': 'yes',
    #                         'type': u'string',
    #                         "term_vector" : "with_positions_offsets"},
    #              u'city': {'boost': 1.0,
    #                         'index': 'analyzed',
    #                         'store': 'yes',
    #                         'type': u'string',
    #                         "term_vector" : "with_positions_offsets"},
    #        }

    #conn.indices.put_mapping("test-type1", {'properties':mapping}, ["test-index"])#定义test-type
    ##conn.indices.put_mapping("test-type2", {"_parent" : {"type" : "test-type1"}}, ["test-index"])#从test-type继承

    ##插入索引数据
    ##{"id":"1", "user_id":"u1", "nick":u"压力很大", "city":u"成都"}: 文档数据
    ##test-index：索引名称
    ##test-type: 类型
    ##1: id 注：id可以不给，系统会自动生成
    #conn.index({"id":"1", "user_id":"u1", "nick":u"美女很多", "city":u"成都"}, "test-index", "test-type1",1)
    #conn.index({"id":"2", "user_id":"u2", "nick":u"压力很小", "city":u"北京"}, "test-index", "test-type1",2)
    #conn.index({"id":"3", "user_id":"u3", "nick":u"没有压力", "city":u"成都"}, "test-index", "test-type1",3)
    #conn.index({"id":"4", "user_id":"u4", "nick":u"东方明珠", "city":u"上海"}, "test-index", "test-type1",4)
    #conn.index({"id":"5", "user_id":"u5", "nick":u"环境优美", "city":u"深圳"}, "test-index", "test-type1",5)
    #conn.index({"id":"6", "user_id":"u6", "nick":u"城市很脏", "city":u"西安"}, "test-index", "test-type1",6)
    
    #conn.default_indices=["test-index"]#设置默认的索引
    #conn.indices.refresh()#刷新以获得最新插入的文档

def query():
    reload(pyes)
    #查询nick中包含压力的记录
    #q = StringQuery(u"压力",'nick')
    q = pyes.query.MatchQuery('nick',u"压力很小",type="phrase_prefix")
    #q = pyes.query.MatchQuery('nick',u"压力很小")
    #q1 = pyes.query.MatchQuery("user_id","u1")
    #q2 = pyes.query.MatchQuery("id","6")
    #q = pyes.query.BoolQuery(should=[q1, q2])
    results = conn.search(q)

    for r in results:
        print u"查询nick中包含压力的记录", r['nick'], r['city'], r['user_id']

    #return r['user_id']


if __name__ == '__main__':
    creat()
    #query()
