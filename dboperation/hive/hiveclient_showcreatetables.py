#!/usr/bin/env python
# -*- coding: utf-8 -*-
# hive util with hive server2
import sys
import pyhs2
import re
import os

reload(sys)
sys.setdefaultencoding("utf-8")

class HiveClient:
    def __init__(self, db_host, user, password, database, port=10000, authMechanism="PLAIN"):
        """
        create connection to hive server2
        """
        self.conn = pyhs2.connect(host=db_host,
                                    port=port,
                                    authMechanism=authMechanism,
                                    user=user,
                                    password=password,
                                    database=database,
                                    )
    def query(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetch()
    def queryDDl(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
    def close(self):
        """
        close connection
        """
        self.conn.close()
def main():
    """
    main process
    @rtype:
    @return:
    @note:
    """
    #hive_client = HiveClient(db_host='10.112.88.223', port=10000, user='hive', password='hive',
    #                         database='test', authMechanism='PLAIN')
    #old cluster
    hive_client = HiveClient(db_host='10.110.93.215', port=10000, user='hive', password='lsjr@2016H',
    #new cluster
    #hive_client = HiveClient(db_host='10.60.138.102', port=10000, user='hive', password='Lfc@2016',
                             database='default', authMechanism='PLAIN')
    #result = hive_client.query('select * from t_test limit 10')
    result = hive_client.query('show databases')
    dir = "databases"
    os.makedirs(dir)
    for databaselist in result:
        for db in databaselist:
            hive_client.queryDDl('use '+db)
            list_table_result = hive_client.query('show tables')
            dbout = open(dir+"/"+db,"w")
            for tablelist in list_table_result:
                for table in tablelist:
                    qresult = hive_client.query('show create table '+table)
                    for item in qresult:
                        joinitem = "\"".join(item)
                        rep = re.compile("`")
                        rep_joinitem = re.sub(rep,"",str(joinitem))
                        dbout.write(rep_joinitem+'\n')
                    dbout.write("\n")
            dbout.close()

    hive_client.close()

if __name__ == '__main__':
    main()
