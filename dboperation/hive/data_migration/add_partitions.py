#!/usr/bin/env python
# -*- coding: utf-8 -*-
# hive util with hive server2
import sys
import pyhs2
import re
import os
import commands

reload(sys)
sys.setdefaultencoding("utf-8")

def remove_empty_file(dir):
    for parent,dirnames,filenames in os.walk(dir):
        for file in filenames:
            try:
                filepath = parent+'/'+file
                filesize = os.path.getsize(filepath)
                print filepath
                print filesize
                if filesize == 0:
                    try:
                        os.remove(filepath)
                    except:
                        pass
            except:
                pass

class HiveClient:
    def __init__(self, db_host, user, password, database, port=10000, authMechanism="PLAIN"):
        """
        create connection to hive server2
        """
        self.conn = pyhs2.connect(host=db_host,
                                    port=port,
                                    authMechanism=authMechanism,
                                    user=user,
                                    password=password,
                                    database=database,
                                    )
    def query(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetch()
    def queryDDl(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
    def close(self):
        """
        close connection
        """
        self.conn.close()

def main():
    """
    main process
    @rtype:
    @return:
    @note:
    """
    #hive_client = HiveClient(db_host='10.112.88.223', port=10000, user='hive', password='hive',
    #                         database='test', authMechanism='PLAIN')

    #test cluster
    hftpip = '10.11.145.168'
    hive_client = HiveClient(db_host='10.11.145.168', port=10000, user='hive', password='hive',
    #old cluster
    #hftpip = '10.110.93.216'
    #hive_client = HiveClient(db_host='10.110.93.215', port=10000, user='hive', password='lsjr@2016H',
    #new cluster
    #hftpip = '10.60.138.101'
    #hive_client = HiveClient(db_host='10.60.138.102', port=10000, user='hive', password='Lfc@2016',
                             database='default', authMechanism='PLAIN')
    #result = hive_client.query('select * from t_test limit 10')
    result = hive_client.query('show databases')
    #tbdir = "./parti_tbdir"
    #if os.path.exists(tbdir):
    #    for parent,dirnames,filenames in os.walk(tbdir):
    #        for file in filenames:
    #            os.remove(tbdir+"/"+file)
    #    os.rmdir(tbdir)
    #os.mkdir(tbdir)

    exclu_dbs = ["default", "lexintest", "sdf_lefinace", "sdf_lepay", "sdf_trade_init", "dwb_lefinace", "dwb_lepay"]
    app_partition_db_list=[]
    dim_partition_db_list=[]
    leco_partition_db_list=[]
    source_partition_db_list=[]
    dwb_partition_db_list=[]
    dwd_partition_db_list=[]

    for databaselist in result:
        for db in databaselist:
            if db not in exclu_dbs:
                hive_client.queryDDl('use '+db)
                list_table_result = hive_client.query('show tables')
                partition_column = dict() 
                partition_tb_list = [] 
                for tablelist in list_table_result:
                    for table in tablelist:
                        partition_column_info = []
                        partition_stmts_scope = "false"
                        qresult = hive_client.query('show create table '+table)
                        for item in qresult:
                            joinitem = "\"".join(item)
                            searchO = re.search(r'.*PARTITIONED BY.*', joinitem, re.M|re.I)
                            if searchO:
                                partition_stmts_scope = "true"
                                partition_tb_list.append(table)
                                if db == "app":
                                    app_partition_db_list.append(table)
                                if db == "dim":
                                    dim_partition_db_list.append(table)
                                if db == "leco":
                                    leco_partition_db_list.append(table)
                                if db == "source":
                                    source_partition_db_list.append(table)
                                if db == "dwb":
                                    dwb_partition_db_list.append(table)
                                if db == "dwd":
                                    dwd_partition_db_list.append(table)
                            rep = re.compile("`")
                            rep_joinitem = re.sub(rep,"",str(joinitem))
                            if partition_stmts_scope == "true":
                                searchPClmn = re.search(r'\s+(\w+) (\w+).*', rep_joinitem, re.M|re.I)
                                searchPClmnEND = re.search(r'\s+(\w+) (\w+).*\)', rep_joinitem, re.M|re.I)
                                if searchPClmnEND:
                                    partition_stmts_scope = "false"
                                if searchPClmn:
                                    column_name = searchPClmn.group(1)
                                    column_type = searchPClmn.group(2)
                                    #print column_name+" "+column_type
                                    partition_column_info.append(column_name+","+column_type)
                        if len(partition_column_info) != 0:
                            partition_column[table] = partition_column_info

                #result = partition_column.get("advanced_tags")
                #print partition_tb_list
                partition_keytype = dict()
                for tb in partition_tb_list:
                    hadoopcmd = "hadoop fs -ls -R hftp://"+hftpip+":50070/user/hive/warehouse/"+db+".db/"+tb
                    hadoopcmdstatus, result = commands.getstatusoutput(hadoopcmd)
                    resultlst = result.split("\n")
                    for line in resultlst:
                        searchDir = re.search(r'.*hftp://.*:50070/user/hive/warehouse/(\w+.db)/(\w+)/(.*)', line, re.M|re.I)
                        if searchDir:
                            partitiondir = searchDir.group(3)
                            dirlevel = partitiondir.count('/')
                            if dirlevel == (len(partition_column[tb])-1):
                                #print partition_column[tb]
                                for item in partition_column[tb]:
                                    [key,value] = item.split(",")
                                    partition_keytype[key] = value
                                #for (k,v) in partition_keytype.items():
                                #    print k,v

                                partitiondir_list = partitiondir.split('/')
                                partition_kv_stmts = ""
                                partition_stmts = ""
                                for item in partitiondir_list:
                                    #print item
                                    [key,value] = item.split("=")
                                    global partition_kv_stmts
                                    if partition_keytype[key] == "string" or partition_keytype[key] == "String" or partition_keytype[key] == "STRING":
                                        partition_kv_stmts = partition_kv_stmts+key+"='"+value+"'"+","
                                    else:
                                        partition_kv_stmts = partition_kv_stmts+key+"="+value+","
                                partition_kv_stmts_list = list(partition_kv_stmts)
                                partition_kv_stmts_list.pop() #delete the last ","
                                partition_kv_stmts = "".join(partition_kv_stmts_list)
                                #print partition_kv_stmts
                                global partition_stmts
                                partition_stmts = partition_stmts+"("+partition_kv_stmts+")"
                                #print partition_stmts
                                add_partition_sql = "alter table "+db+"."+tb+" add if not exists partition "+partition_stmts+" location '/user/hive/warehouse/"+db+".db/"+tb+"/"+partitiondir+"'"
                                hive_client.queryDDl(add_partition_sql)
                                #print add_partition_sql
                                #print partitiondir


    hive_client.close()

    dir = "./parti_tbdir"
    #remove_empty_file(dir)

if __name__ == '__main__':
    main()
