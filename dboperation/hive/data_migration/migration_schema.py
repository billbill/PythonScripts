#!/usr/bin/env python
# -*- coding: utf-8 -*-
# hive util with hive server2
import sys
import pyhs2
import re
import os

reload(sys)
sys.setdefaultencoding("utf-8")

class HiveClient:
    def __init__(self, db_host, user, password, database, port=10000, authMechanism="PLAIN"):
        """
        create connection to hive server2
        """
        self.conn = pyhs2.connect(host=db_host,
                                    port=port,
                                    authMechanism=authMechanism,
                                    user=user,
                                    password=password,
                                    database=database,
                                    )
    def query(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetch()
    def queryDDl(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
    def close(self):
        """
        close connection
        """
        self.conn.close()
def main():
    """
    main process
    @rtype:
    @return:
    @note:
    """
    #hive_client = HiveClient(db_host='10.112.88.223', port=10000, user='hive', password='hive',
    #                         database='test', authMechanism='PLAIN')
    #old cluster
    hive_client = HiveClient(db_host='10.110.93.215', port=10000, user='hive', password='lsjr@2016H',
    #new cluster
    #hive_client = HiveClient(db_host='10.60.138.102', port=10000, user='hive', password='Lfc@2016',
                             database='default', authMechanism='PLAIN')
    #result = hive_client.query('select * from t_test limit 10')
    result = hive_client.query('show databases')
    sqldir = "./migration_schema"
    if os.path.exists(sqldir):
        for parent,dirnames,filenames in os.walk(sqldir):
            for file in filenames:
                os.remove(sqldir+"/"+file)
        os.rmdir(sqldir)
    os.mkdir(sqldir)

    mapping_dir = "./table_mapping"

    for parent,dirnames,filenames in os.walk(mapping_dir):
        for file in filenames:
            ifile = open(mapping_dir+"/"+file,"r")
            matchF = re.match("(.*)\.mapping",str(file))
            if matchF:
                outsql = open(sqldir+"/"+"migrate_"+matchF.group(1)+"_schema"+".sql","w")
                lecosql = open(sqldir+"/"+"migrate_"+matchF.group(1)+"_schema_toleco"+".sql","w")
            #outsql = open(sqldir+"/"+file+".sql","w")
            while True:
                line = ifile.readline()
                if not line:
                    break
                matchO = re.match("(.*),(.*),(.*),(.*)", line)
                if matchO:
                    srcdb = matchO.group(1)
                    srctb = matchO.group(2)
                    dstdb = matchO.group(3)
                    dsttb = matchO.group(4)
                    hive_client.queryDDl('use '+srcdb)
                    if dstdb != "-":
                        if dstdb == "leco":
                            lecosql.write("use "+dstdb+";"+"\n")
                        else:
                            outsql.write("use "+dstdb+";"+"\n")
                        if not srctb.startswith("?"):
                            src_create_result = hive_client.query('show create table '+srctb)
                            for item in src_create_result:
                                joinitem = "\"".join(item)
                                rep = re.compile("`")
                                rep_joinitem = re.sub(rep,"",str(joinitem))
                                rep = re.compile("nameservice1")
                                repnamespace = re.sub(rep,"nameservice2",str(rep_joinitem))
                                if repnamespace.startswith("  'hdfs:"):
                                    #print "B: %s" % repnamespace
                                    repnamespace = repnamespace.replace(srctb,dsttb)
                                    #print "A: %s" % repnamespace
                                    if dstdb == "leco":
                                        repnamespace = repnamespace.replace(srcdb+'.db','leco.db')
                                        #lecosql.write(repnamespace+'\n')
                                        #print repnamespace
                                matchCET = re.match("(CREATE EXTERNAL TABLE )(.*)(\()", repnamespace)
                                if matchCET:
                                    tmpcreatestmts = matchCET.group(1)+dsttb+matchCET.group(3)
                                    rep = re.compile("CREATE EXTERNAL TABLE")
                                    newcreatestmtline = re.sub(rep,"CREATE EXTERNAL TABLE IF NOT EXISTS",str(tmpcreatestmts))
                                    #print newcreatestmtline
                                    if dstdb == "leco":
                                        lecosql.write(newcreatestmtline+'\n')
                                    else:
                                        outsql.write(newcreatestmtline+'\n')
                                else:
                                    matchCT = re.match("(CREATE TABLE )(.*)(\()", repnamespace)
                                    if matchCT:
                                        tmpcreatestmts = matchCT.group(1)+dsttb+matchCT.group(3)
                                        rep = re.compile("CREATE TABLE")
                                        newcreatestmtline = re.sub(rep,"CREATE TABLE IF NOT EXISTS",str(tmpcreatestmts))
                                        #print newcreatestmtline
                                        if dstdb == "leco":
                                            lecosql.write(newcreatestmtline+'\n')
                                        else:
                                            outsql.write(newcreatestmtline+'\n')
                                    else:
                                        #print repnamespace
                                        if dstdb == "leco":
                                            lecosql.write(repnamespace+'\n')
                                        else:
                                            outsql.write(repnamespace+'\n')
                            if dstdb == "leco":
                                lecosql.write(";")
                                lecosql.write("\n")
                                lecosql.write("\n")
                            else:
                                outsql.write(";")
                                outsql.write("\n")
                                outsql.write("\n")
            outsql.close()
            lecosql.close()

    hive_client.close()

if __name__ == '__main__':
    main()
