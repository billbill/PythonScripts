#!/usr/bin/env python
# -*- coding: utf-8 -*-
# hive util with hive server2
import sys
import pyhs2
import re
import os

reload(sys)
sys.setdefaultencoding("utf-8")

def remove_empty_file(dir):
    for parent,dirnames,filenames in os.walk(dir):
        for file in filenames:
            try:
                filepath = parent+'/'+file
                filesize = os.path.getsize(filepath)
                #print filepath
                #print filesize
                if filesize == 0:
                    try:
                        os.remove(filepath)
                    except:
                        pass
            except:
                pass

class HiveClient:
    def __init__(self, db_host, user, password, database, port=10000, authMechanism="PLAIN"):
        """
        create connection to hive server2
        """
        self.conn = pyhs2.connect(host=db_host,
                                    port=port,
                                    authMechanism=authMechanism,
                                    user=user,
                                    password=password,
                                    database=database,
                                    )
    def query(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetch()
    def queryDDl(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
    def close(self):
        """
        close connection
        """
        self.conn.close()
def main():
    """
    main process
    @rtype:
    @return:
    @note:
    """
    #hive_client = HiveClient(db_host='10.112.88.223', port=10000, user='hive', password='hive',
    #                         database='test', authMechanism='PLAIN')
    #old cluster
    #hive_client = HiveClient(db_host='10.110.93.215', port=10000, user='hive', password='lsjr@2016H',
    #new cluster
    hive_client = HiveClient(db_host='10.60.138.102', port=10000, user='hive', password='Lfc@2016',
                             database='default', authMechanism='PLAIN')
    #result = hive_client.query('select * from t_test limit 10')
    result = hive_client.query('show databases')
    shdir = "./load_partitions"
    if os.path.exists(shdir):
        for parent,dirnames,filenames in os.walk(shdir):
            for file in filenames:
                os.remove(shdir+"/"+file)
        os.rmdir(shdir)
    os.mkdir(shdir)

    exclu_dbs = ["default", "lexintest", "sdf_lefinace", "sdf_lepay", "sdf_trade_init", "dwb_lefinace", "dwb_lepay"]

    for databaselist in result:
        for db in databaselist:
            if db not in exclu_dbs:
                dbout = open(shdir+"/"+"load_partitions"+db+".sh","w")
                srcdb = db
                var_src_db = "srcdb="+srcdb

                hive_client.queryDDl('use '+db)
                list_table_result = hive_client.query('show tables')
                if len(list_table_result) > 0:
                    dbout.write("#################################################################################\n")
                    dbout.write(var_src_db+'\n')
                for tablelist in list_table_result:
                    for table in tablelist:
                        srctb = table
                        var_src_tb = "srctb="+srctb
                        #hdfs_path = '''hdfs_path="/user/hive/warehouse/${srcdb}.db/${srctb}/${partition}"'''

                        dtsh = '''
datebeg=2016-03-01
dateend=2016-11-06
beg_s=`date -d "$datebeg" +%s`
end_s=`date -d "$dateend" +%s`
while [ "$beg_s" -le "$end_s" ]   
    do
    dt=$(date -d @$beg_s +"%Y-%m-%d")
    beg_s=$((beg_s+86400))
    partition="dt=${dt}"
    parent_path="/user/hive/warehouse/${srcdb}.db/${srctb}"
    hdfs_path="/user/hive/warehouse/${srcdb}.db/${srctb}/${partition}"'''
                        hivesql = '''
    hive -e "
        use ${srcdb};
        ALTER TABLE $srctb ADD IF NOT EXISTS PARTITION (dt='${dt}') LOCATION '${hdfs_path}';"
'''
                        checkdir_stmts = '''
    # check the partition directory exists or not
    partitionexistFlag=`hadoop fs -ls ${parent_path} | grep "${partition}"| wc -l`
    if [[ ${partitionexistFlag} == "0" ]] ; then
        echo "${hdfs_path} doesn't exist"
    else
        hive -e "
            use ${srcdb};
            ALTER TABLE $srctb ADD IF NOT EXISTS PARTITION (dt='${dt}') LOCATION '${hdfs_path}';"
    fi'''
                        dbout.write(var_src_tb+'\n')
                        dbout.write(dtsh+'\n')
                        dbout.write(checkdir_stmts+'\n')
                        dbout.write("done\n")
                        dtsh1 = '''
datebeg=20160301
dateend=20161106
beg_s=`date -d "$datebeg" +%s`
end_s=`date -d "$dateend" +%s`
while [ "$beg_s" -le "$end_s" ]   
    do
    dt=$(date -d @$beg_s +"%Y%m%d")
    beg_s=$((beg_s+86400))
    partition="dt=${dt}"
    parent_path="/user/hive/warehouse/${srcdb}.db/${srctb}"
    hdfs_path="/user/hive/warehouse/${srcdb}.db/${srctb}/${partition}"'''
                        hivesql = '''
    hive -e "
        use ${srcdb};
        ALTER TABLE $srctb ADD IF NOT EXISTS PARTITION (dt='${dt}') LOCATION '${hdfs_path}';"
'''
                        checkdir_stmts1 = '''
    # check the partition directory exists or not
    partitionexistFlag=`hadoop fs -ls ${parent_path} | grep "${partition}"| wc -l`
    if [[ ${partitionexistFlag} == "0" ]] ; then
        echo "${hdfs_path} doesn't exist"
    else
        hive -e "
            use ${srcdb};
            ALTER TABLE $srctb ADD IF NOT EXISTS PARTITION (dt='${dt}') LOCATION '${hdfs_path}';"
    fi'''
                        dbout.write(dtsh1+'\n')
                        dbout.write(checkdir_stmts1+'\n')
                        dbout.write("done\n")
                        dbout.write("#################################################################################\n")
                        dbout.write("\n")
                        dbout.write("\n")
                        #qresult = hive_client.query('show create table '+table)
                dbout.close()

    hive_client.close()

    dir = "./load_partitions"
    remove_empty_file(dir)

if __name__ == '__main__':
    main()
