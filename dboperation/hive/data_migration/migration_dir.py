#!/usr/bin/env python
# -*- coding: utf-8 -*-
# hive util with hive server2
import sys
import pyhs2
import re
import os

reload(sys)
sys.setdefaultencoding("utf-8")

def remove_empty_file(dir):
    for parent,dirnames,filenames in os.walk(dir):
        for file in filenames:
            try:
                filepath = parent+'/'+file
                filesize = os.path.getsize(filepath)
                #print filepath
                #print filesize
                if filesize == 0:
                    try:
                        os.remove(filepath)
                    except:
                        pass
            except:
                pass

class HiveClient:
    def __init__(self, db_host, user, password, database, port=10000, authMechanism="PLAIN"):
        """
        create connection to hive server2
        """
        self.conn = pyhs2.connect(host=db_host,
                                    port=port,
                                    authMechanism=authMechanism,
                                    user=user,
                                    password=password,
                                    database=database,
                                    )
    def query(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetch()
    def queryDDl(self, sql):
        """
        query
        """
        with self.conn.cursor() as cursor:
            cursor.execute(sql)
    def close(self):
        """
        close connection
        """
        self.conn.close()
def main():
    """
    main process
    @rtype:
    @return:
    @note:
    """
    #hive_client = HiveClient(db_host='10.112.88.223', port=10000, user='hive', password='hive',
    #                         database='test', authMechanism='PLAIN')
    #old cluster
    hive_client = HiveClient(db_host='10.110.93.215', port=10000, user='hive', password='lsjr@2016H',
    #new cluster
    #hive_client = HiveClient(db_host='10.60.138.102', port=10000, user='hive', password='Lfc@2016',
                             database='default', authMechanism='PLAIN')
    #result = hive_client.query('select * from t_test limit 10')
    result = hive_client.query('show databases')
    shdir = "./migration_dir"
    if os.path.exists(shdir):
        for parent,dirnames,filenames in os.walk(shdir):
            for file in filenames:
                os.remove(shdir+"/"+file)
        os.rmdir(shdir)
    os.mkdir(shdir)

    mapping_dir = "./table_mapping"

    exclu_dbs = ["default", "lexintest", "sdf_lefinace", "sdf_lepay", "sdf_trade_init", "dwb_lefinace", "dwb_lepay"]
    app_partition_db_list=[]
    dim_partition_db_list=[]
    leco_partition_db_list=[]
    source_partition_db_list=[]
    dwb_partition_db_list=[]
    dwd_dts_partition_db_list=[]
    dwd_dtint_partition_db_list=[]
    dwd_cc_partition_db_list=[]
    dwd_country_partition_db_list=[]
    for databaselist in result:
        for db in databaselist:
            if db not in exclu_dbs:
                hive_client.queryDDl('use '+db)
                list_table_result = hive_client.query('show tables')
                partitions_flag = "false"
                dts_partitions_flag = "false"
                dtint_partitions_flag = "false"
                cc_partitions_flag = "false"
                country_partitions_flag = "false"
                for tablelist in list_table_result:
                    for table in tablelist:
                        qresult = hive_client.query('show create table '+table)
                        for item in qresult:
                            joinitem = "\"".join(item)
                            searchO = re.search(r'.*PARTITIONED BY.*', joinitem, re.M|re.I)
                            if searchO:
                                partitions_flag = "true"
                            rep = re.compile("`")
                            rep_joinitem = re.sub(rep,"",str(joinitem))
                            rep = re.compile("nameservice1")
                            repnamespace = re.sub(rep,"nameservice2",str(rep_joinitem))
                            if partitions_flag == "true":
                                searchDTS = re.search(r"  dt string.*", repnamespace, re.M|re.I)
                                searchDTINT = re.search(r"  dt .*", repnamespace, re.M|re.I)
                                searchCC = re.search(r"  cc string.*", repnamespace, re.M|re.I)
                                searchCountry = re.search(r"  country_code string.*", repnamespace, re.M|re.I)
                                searchTB = re.search(r"  'hdfs:.*\/(\w+)'", repnamespace, re.M|re.I)
                                if searchDTS:
                                    dts_partitions_flag = "true"
                                if searchDTINT:
                                    dtint_partitions_flag = "true"
                                if searchCC:
                                    cc_partitions_flag = "true"
                                if searchCountry:
                                    country_partitions_flag = "true"
                                if searchTB:
                                    if db == "app":
                                        app_partition_db_list.append(searchTB.group(1))
                                    if db == "dim":
                                        dim_partition_db_list.append(searchTB.group(1))
                                    if db == "leco":
                                        leco_partition_db_list.append(searchTB.group(1))
                                    if db == "source":
                                        source_partition_db_list.append(searchTB.group(1))
                                    if db == "dwb":
                                        dwb_partition_db_list.append(searchTB.group(1))
                                    if db == "dwd":
                                        if (dts_partitions_flag == "true" and cc_partitions_flag == "false") or (dts_partitions_flag == "true" and country_partitions_flag == "false"):
                                            dwd_dts_partition_db_list.append(searchTB.group(1))
                                        if (dtint_partitions_flag == "true" and cc_partitions_flag == "false") or (dtint_partitions_flag == "true" and country_partitions_flag == "false"):
                                            dwd_dtint_partition_db_list.append(searchTB.group(1))
                                        if cc_partitions_flag == "true":
                                            dwd_cc_partition_db_list.append(searchTB.group(1))
                                        if country_partitions_flag == "true":
                                            dwd_country_partition_db_list.append(searchTB.group(1))
                        partitions_flag = "false"
                        dts_partitions_flag = "false"
                        dtint_partitions_flag = "false"
                        cc_partitions_flag = "false"
                        country_partitions_flag = "false"

    #for item in dwb_partition_db_list:
    #    print "dwb:%s" % item
    #for item in dwd_partition_db_list:
    #    print "dwd:%s" % item

    for parent,dirnames,filenames in os.walk(mapping_dir):
        for file in filenames:
            ifile = open(mapping_dir+"/"+file,"r")
            matchF = re.match("(.*)\.mapping",str(file))
            if matchF:
                outsh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_dir"+".sh","w")
                loansh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_loan_dir"+".sh","w")
                finsh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_fin_dir"+".sh","w")
                paysh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_pay_dir"+".sh","w")
                risksh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_risk_dir"+".sh","w")
                accountingsh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_accounting_dir"+".sh","w")
                assetssh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_assets_dir"+".sh","w")
                tradesh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_trade_dir"+".sh","w")
                supchainsh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_supchain_dir"+".sh","w")

                lecooutsh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_dir_toleco"+".sh","w")
                lecoloansh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_loan_dir_toleco"+".sh","w")
                lecofinsh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_fin_dir_toleco"+".sh","w")
                lecopaysh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_pay_dir_toleco"+".sh","w")
                lecorisksh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_risk_dir_toleco"+".sh","w")
                lecoaccountingsh = open(shdir+"/"+"migrate_"+matchF.group(1)+"_accounting_dir_toleco"+".sh","w")
                lecoassetssh = open(shdir+"/"+matchF.group(1)+"_assets_dir_toleco"+".sh","w")
                lecotradesh = open(shdir+"/"+matchF.group(1)+"_trade_dir_toleco"+".sh","w")
                lecosupchainsh = open(shdir+"/"+matchF.group(1)+"_supchain_dir_toleco"+".sh","w")
            #outsh = open(shdir+"/"+file+".sql","w")
            while True:
                line = ifile.readline()
                if not line:
                    break
                matchO = re.match("(.*),(.*),(.*),(.*)", line)
                if matchO:
                    srcdb = matchO.group(1)
                    srctb = matchO.group(2)
                    dstdb = matchO.group(3)
                    dsttb = matchO.group(4)
                    hive_client.queryDDl('use '+srcdb)
                    if dstdb != "-":
                        if not srctb.startswith("?"):
                            dtsh=''''''
                            country_codesh=''''''
                            ccsh=''''''
                            done=""
                            var_src_db = "srcdb="+srcdb
                            var_src_tb = "srctb="+srctb
                            var_dst_db = "dstdb="+dstdb
                            var_dst_tb = "dsttb="+dsttb
                            srcparent_path = '''srcparent_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}"'''
                            dstparent_path = '''dstparent_path="/user/hive/warehouse/${dstdb}.db/${dsttb}"'''
                            echosh = '''
echo "srctb:${srctb}"
echo "dsttb:${dsttb}"
echo "partition:${partition}"
echo "src_path:${src_path}"
echo "dst_path:${dst_path}"
                            '''
                            if srctb in dwb_partition_db_list:
                                dtsh = '''
datebeg=2016-03-01
dateend=2016-11-10
beg_s=`date -d "$datebeg" +%s`
end_s=`date -d "$dateend" +%s`
while [ "$beg_s" -le "$end_s" ]   
do
dt=$(date -d @$beg_s +"%Y-%m-%d")
beg_s=$((beg_s+86400))
partition="dt='${dt}'"
partitionHdfs="dt=${dt}"'''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/${partitionHdfs}"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/${partitionHdfs}"'''
                                done="done"
                            elif srctb in app_partition_db_list:
                                dtsh = '''
datebeg=2016-03-01
dateend=2016-11-10
beg_s=`date -d "$datebeg" +%s`
end_s=`date -d "$dateend" +%s`
while [ "$beg_s" -le "$end_s" ]   
do
dt=$(date -d @$beg_s +"%Y-%m-%d")
beg_s=$((beg_s+86400))
partition="dt='${dt}'"
partitionHdfs="dt=${dt}"'''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/${partitionHdfs}"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/${partitionHdfs}"'''
                                done="done"
                            elif srctb in dim_partition_db_list:
                                dtsh = '''
datebeg=2016-03-01
dateend=2016-11-10
beg_s=`date -d "$datebeg" +%s`
end_s=`date -d "$dateend" +%s`
while [ "$beg_s" -le "$end_s" ]   
do
dt=$(date -d @$beg_s +"%Y-%m-%d")
beg_s=$((beg_s+86400))
partition="dt='${dt}'"
partitionHdfs="dt=${dt}"'''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/${partitionHdfs}"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/${partitionHdfs}"'''
                                done="done"
                            elif srctb in leco_partition_db_list:
                                dtsh = '''
datebeg=2016-03-01
dateend=2016-11-10
beg_s=`date -d "$datebeg" +%s`
end_s=`date -d "$dateend" +%s`
while [ "$beg_s" -le "$end_s" ]   
do
dt=$(date -d @$beg_s +"%Y-%m-%d")
beg_s=$((beg_s+86400))
partition="dt='${dt}'"
partitionHdfs="dt=${dt}"'''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/${partitionHdfs}"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/${partitionHdfs}"'''
                                done="done"
                            elif srctb in source_partition_db_list:
                                dtsh = '''
datebeg=2016-03-01
dateend=2016-11-10
beg_s=`date -d "$datebeg" +%s`
end_s=`date -d "$dateend" +%s`
while [ "$beg_s" -le "$end_s" ]   
do
dt=$(date -d @$beg_s +"%Y-%m-%d")
beg_s=$((beg_s+86400))
partition="dt='${dt}'"
partitionHdfs="dt=${dt}"'''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/${partitionHdfs}"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/${partitionHdfs}"'''
                                done="done"
                            elif srctb in dwd_dts_partition_db_list:
                                dtsh = '''
datebeg=2016-03-01
dateend=2016-11-10
beg_s=`date -d "$datebeg" +%s`
end_s=`date -d "$dateend" +%s`
while [ "$beg_s" -le "$end_s" ]   
do
dt=$(date -d @$beg_s +"%Y-%m-%d")
beg_s=$((beg_s+86400))
partition="dt='${dt}'"
partitionHdfs="dt=${dt}"'''
                                country_codesh = ''''''
                                ccsh = ''''''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/${partitionHdfs}"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/${partitionHdfs}"'''
                                done="done"
                            elif srctb in dwd_dtint_partition_db_list:
                                dtsh = '''
datebeg=20160301
dateend=20161110
beg_s=`date -d "$datebeg" +%s`
end_s=`date -d "$dateend" +%s`
while [ "$beg_s" -le "$end_s" ]   
do
dt=$(date -d @$beg_s +"%Y%m%d")
beg_s=$((beg_s+86400))
partition="dt='${dt}'"
partitionHdfs="dt=${dt}"'''
                                country_codesh = ''''''
                                ccsh = ''''''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/${partitionHdfs}"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/${partitionHdfs}"'''
                                done="done"
                            elif srctb in dwd_cc_partition_db_list:
                                dtsh = ''''''
                                country_codesh = ''''''
                                ccsh = '''
for cc in cn us;
do
partition="cc='${cc}'"
partitionHdfs="cc=${cc}"'''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/${partitionHdfs}"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/${partitionHdfs}"'''
                                done="done"
                            elif srctb in dwd_country_partition_db_list:
                                dtsh = ''''''
                                country_codesh = '''
for country_code in cn us;
do
partition="country_code='${country_code}'"
partitionHdfs="country_code=${country_code}"'''
                                ccsh = ''''''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/${partitionHdfs}"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/${partitionHdfs}"'''
                                done="done"
                            else:
                                dtsh=''''''
                                country_codesh=''''''
                                ccsh=''''''
                                src_path_def = '''src_path="hftp://10.110.93.216:50070/user/hive/warehouse/${srcdb}.db/${srctb}/*"'''
                                dst_path_def = '''dst_path="/user/hive/warehouse/${dstdb}.db/${dsttb}/"'''
                                done=""

                            checkanddistcp_stmts = '''
# check the source partition directory exists or not
srcexistFlag=`hadoop fs -ls ${srcparent_path} | grep "${partitionHdfs}"| wc -l`
if [[ ${srcexistFlag} == "0" ]] ; then
    echo "${src_path} doesn't exist"
else
    # check the destination partition directory exists or not
    dstexistFlag=`hadoop fs -ls ${dstparent_path} | grep "${partitionHdfs}"| wc -l`
    if [[ ${dstexistFlag} == "0" ]] ; then
       hadoop fs -mkdir -p ${dstparent_path}/${partitionHdfs}
    fi
    echo "拷贝数据......"
    echo "hadoop distcp -pb -overwrite ${src_path} ${dst_path}"
    hadoop distcp -pb -overwrite ${src_path} ${dst_path}
fi'''
                            echo_stmts = '''echo "拷贝数据......"'''
                            matchDTB = re.match("([a-zA-Z0-9]+)_([a-zA-Z0-9]+)_([a-zA-Z0-9]+)", dsttb)
                            if matchDTB:
                                if matchDTB.group(3) == "loan":
                                    if dstdb == "leco":
                                        lecoloansh.write(var_src_db+'\n')
                                        lecoloansh.write(var_src_tb+'\n')
                                        lecoloansh.write(var_dst_db+'\n')
                                        lecoloansh.write(var_dst_tb+'\n')
                                        lecoloansh.write(dtsh+'\n')
                                        lecoloansh.write(country_codesh+'\n')
                                        lecoloansh.write(ccsh+'\n')
                                        lecoloansh.write(src_path_def+'\n')
                                        lecoloansh.write(dst_path_def+'\n')
                                        lecoloansh.write(srcparent_path+'\n')
                                        lecoloansh.write(dstparent_path+'\n')
                                        lecoloansh.write(echosh+'\n')
                                        lecoloansh.write(checkanddistcp_stmts+'\n')
                                        lecoloansh.write(done+'\n')
                                        lecoloansh.write("\n")
                                        lecoloansh.write("#################################################\n")
                                    else:
                                        loansh.write(var_src_db+'\n')
                                        loansh.write(var_src_tb+'\n')
                                        loansh.write(var_dst_db+'\n')
                                        loansh.write(var_dst_tb+'\n')
                                        loansh.write(dtsh+'\n')
                                        loansh.write(country_codesh+'\n')
                                        loansh.write(ccsh+'\n')
                                        loansh.write(src_path_def+'\n')
                                        loansh.write(dst_path_def+'\n')
                                        loansh.write(srcparent_path+'\n')
                                        loansh.write(dstparent_path+'\n')
                                        loansh.write(echosh+'\n')
                                        loansh.write(checkanddistcp_stmts+'\n')
                                        loansh.write(done+'\n')
                                        loansh.write("\n")
                                        loansh.write("#################################################\n")
                                elif matchDTB.group(3) == "fin":
                                    if dstdb == "leco":
                                        lecofinsh.write(var_src_db+'\n')
                                        lecofinsh.write(var_src_tb+'\n')
                                        lecofinsh.write(var_dst_db+'\n')
                                        lecofinsh.write(var_dst_tb+'\n')
                                        lecofinsh.write(dtsh+'\n')
                                        lecofinsh.write(country_codesh+'\n')
                                        lecofinsh.write(ccsh+'\n')
                                        lecofinsh.write(src_path_def+'\n')
                                        lecofinsh.write(dst_path_def+'\n')
                                        lecofinsh.write(srcparent_path+'\n')
                                        lecofinsh.write(dstparent_path+'\n')
                                        lecofinsh.write(echosh+'\n')
                                        lecofinsh.write(checkanddistcp_stmts+'\n')
                                        lecofinsh.write(done+'\n')
                                        lecofinsh.write("\n")
                                        lecofinsh.write("#################################################\n")
                                    else:
                                        finsh.write(var_src_db+'\n')
                                        finsh.write(var_src_tb+'\n')
                                        finsh.write(var_dst_db+'\n')
                                        finsh.write(var_dst_tb+'\n')
                                        finsh.write(dtsh+'\n')
                                        finsh.write(country_codesh+'\n')
                                        finsh.write(ccsh+'\n')
                                        finsh.write(src_path_def+'\n')
                                        finsh.write(dst_path_def+'\n')
                                        finsh.write(srcparent_path+'\n')
                                        finsh.write(dstparent_path+'\n')
                                        finsh.write(echosh+'\n')
                                        finsh.write(checkanddistcp_stmts+'\n')
                                        finsh.write(done+'\n')
                                        finsh.write("\n")
                                        finsh.write("#################################################\n")
                                elif matchDTB.group(3) == "pay":
                                    if dstdb == "leco":
                                        lecopaysh.write(var_src_db+'\n')
                                        lecopaysh.write(var_src_tb+'\n')
                                        lecopaysh.write(var_dst_db+'\n')
                                        lecopaysh.write(var_dst_tb+'\n')
                                        lecopaysh.write(dtsh+'\n')
                                        lecopaysh.write(country_codesh+'\n')
                                        lecopaysh.write(ccsh+'\n')
                                        lecopaysh.write(src_path_def+'\n')
                                        lecopaysh.write(dst_path_def+'\n')
                                        lecopaysh.write(srcparent_path+'\n')
                                        lecopaysh.write(dstparent_path+'\n')
                                        lecopaysh.write(echosh+'\n')
                                        lecopaysh.write(checkanddistcp_stmts+'\n')
                                        lecopaysh.write(done+'\n')
                                        lecopaysh.write("\n")
                                        lecopaysh.write("#################################################\n")
                                    else:
                                        paysh.write(var_src_db+'\n')
                                        paysh.write(var_src_tb+'\n')
                                        paysh.write(var_dst_db+'\n')
                                        paysh.write(var_dst_tb+'\n')
                                        paysh.write(dtsh+'\n')
                                        paysh.write(country_codesh+'\n')
                                        paysh.write(ccsh+'\n')
                                        paysh.write(src_path_def+'\n')
                                        paysh.write(dst_path_def+'\n')
                                        paysh.write(srcparent_path+'\n')
                                        paysh.write(dstparent_path+'\n')
                                        paysh.write(echosh+'\n')
                                        paysh.write(checkanddistcp_stmts+'\n')
                                        paysh.write(done+'\n')
                                        paysh.write("\n")
                                        paysh.write("#################################################\n")
                                elif matchDTB.group(3) == "risk":
                                    if dstdb == "leco":
                                        lecorisksh.write(var_src_db+'\n')
                                        lecorisksh.write(var_src_tb+'\n')
                                        lecorisksh.write(var_dst_db+'\n')
                                        lecorisksh.write(var_dst_tb+'\n')
                                        lecorisksh.write(dtsh+'\n')
                                        lecorisksh.write(country_codesh+'\n')
                                        lecorisksh.write(ccsh+'\n')
                                        lecorisksh.write(src_path_def+'\n')
                                        lecorisksh.write(dst_path_def+'\n')
                                        lecorisksh.write(srcparent_path+'\n')
                                        lecorisksh.write(dstparent_path+'\n')
                                        lecorisksh.write(echosh+'\n')
                                        lecorisksh.write(checkanddistcp_stmts+'\n')
                                        lecorisksh.write(done+'\n')
                                        lecorisksh.write("\n")
                                        lecorisksh.write("#################################################\n")
                                    else:
                                        risksh.write(var_src_db+'\n')
                                        risksh.write(var_src_tb+'\n')
                                        risksh.write(var_dst_db+'\n')
                                        risksh.write(var_dst_tb+'\n')
                                        risksh.write(dtsh+'\n')
                                        risksh.write(country_codesh+'\n')
                                        risksh.write(ccsh+'\n')
                                        risksh.write(src_path_def+'\n')
                                        risksh.write(dst_path_def+'\n')
                                        risksh.write(srcparent_path+'\n')
                                        risksh.write(dstparent_path+'\n')
                                        risksh.write(echosh+'\n')
                                        risksh.write(checkanddistcp_stmts+'\n')
                                        risksh.write(done+'\n')
                                        risksh.write("\n")
                                        risksh.write("#################################################\n")
                                elif matchDTB.group(3) == "accounting":
                                    if dstdb == "leco":
                                        lecoaccountingsh.write(var_src_db+'\n')
                                        lecoaccountingsh.write(var_src_tb+'\n')
                                        lecoaccountingsh.write(var_dst_db+'\n')
                                        lecoaccountingsh.write(var_dst_tb+'\n')
                                        lecoaccountingsh.write(dtsh+'\n')
                                        lecoaccountingsh.write(country_codesh+'\n')
                                        lecoaccountingsh.write(ccsh+'\n')
                                        lecoaccountingsh.write(src_path_def+'\n')
                                        lecoaccountingsh.write(dst_path_def+'\n')
                                        lecoaccountingsh.write(srcparent_path+'\n')
                                        lecoaccountingsh.write(dstparent_path+'\n')
                                        lecoaccountingsh.write(echosh+'\n')
                                        lecoaccountingsh.write(checkanddistcp_stmts+'\n')
                                        lecoaccountingsh.write(done+'\n')
                                        lecoaccountingsh.write("\n")
                                        lecoaccountingsh.write("#################################################\n")
                                    else:
                                        accountingsh.write(var_src_db+'\n')
                                        accountingsh.write(var_src_tb+'\n')
                                        accountingsh.write(var_dst_db+'\n')
                                        accountingsh.write(var_dst_tb+'\n')
                                        accountingsh.write(dtsh+'\n')
                                        accountingsh.write(country_codesh+'\n')
                                        accountingsh.write(ccsh+'\n')
                                        accountingsh.write(src_path_def+'\n')
                                        accountingsh.write(dst_path_def+'\n')
                                        accountingsh.write(srcparent_path+'\n')
                                        accountingsh.write(dstparent_path+'\n')
                                        accountingsh.write(echosh+'\n')
                                        accountingsh.write(checkanddistcp_stmts+'\n')
                                        accountingsh.write(done+'\n')
                                        accountingsh.write("\n")
                                        accountingsh.write("#################################################\n")
                                elif matchDTB.group(3) == "assets":
                                    if dstdb == "leco":
                                        lecoassetssh.write(var_src_db+'\n')
                                        lecoassetssh.write(var_src_tb+'\n')
                                        lecoassetssh.write(var_dst_db+'\n')
                                        lecoassetssh.write(var_dst_tb+'\n')
                                        lecoassetssh.write(dtsh+'\n')
                                        lecoassetssh.write(country_codesh+'\n')
                                        lecoassetssh.write(ccsh+'\n')
                                        lecoassetssh.write(src_path_def+'\n')
                                        lecoassetssh.write(dst_path_def+'\n')
                                        lecoassetssh.write(srcparent_path+'\n')
                                        lecoassetssh.write(dstparent_path+'\n')
                                        lecoassetssh.write(echosh+'\n')
                                        lecoassetssh.write(checkanddistcp_stmts+'\n')
                                        lecoassetssh.write(done+'\n')
                                        lecoassetssh.write("\n")
                                        lecoassetssh.write("#################################################\n")
                                    else:
                                        assetssh.write(var_src_db+'\n')
                                        assetssh.write(var_src_tb+'\n')
                                        assetssh.write(var_dst_db+'\n')
                                        assetssh.write(var_dst_tb+'\n')
                                        assetssh.write(dtsh+'\n')
                                        assetssh.write(country_codesh+'\n')
                                        assetssh.write(ccsh+'\n')
                                        assetssh.write(src_path_def+'\n')
                                        assetssh.write(dst_path_def+'\n')
                                        assetssh.write(srcparent_path+'\n')
                                        assetssh.write(dstparent_path+'\n')
                                        assetssh.write(echosh+'\n')
                                        assetssh.write(checkanddistcp_stmts+'\n')
                                        assetssh.write(done+'\n')
                                        assetssh.write("\n")
                                        assetssh.write("#################################################\n")
                                elif matchDTB.group(3) == "trade":
                                    if dstdb == "leco":
                                        lecotradesh.write(var_src_db+'\n')
                                        lecotradesh.write(var_src_tb+'\n')
                                        lecotradesh.write(var_dst_db+'\n')
                                        lecotradesh.write(var_dst_tb+'\n')
                                        lecotradesh.write(dtsh+'\n')
                                        lecotradesh.write(country_codesh+'\n')
                                        lecotradesh.write(ccsh+'\n')
                                        lecotradesh.write(src_path_def+'\n')
                                        lecotradesh.write(dst_path_def+'\n')
                                        lecotradesh.write(srcparent_path+'\n')
                                        lecotradesh.write(dstparent_path+'\n')
                                        lecotradesh.write(echosh+'\n')
                                        lecotradesh.write(checkanddistcp_stmts+'\n')
                                        lecotradesh.write(done+'\n')
                                        lecotradesh.write("\n")
                                        lecotradesh.write("#################################################\n")
                                    else:
                                        tradesh.write(var_src_db+'\n')
                                        tradesh.write(var_src_tb+'\n')
                                        tradesh.write(var_dst_db+'\n')
                                        tradesh.write(var_dst_tb+'\n')
                                        tradesh.write(dtsh+'\n')
                                        tradesh.write(country_codesh+'\n')
                                        tradesh.write(ccsh+'\n')
                                        tradesh.write(src_path_def+'\n')
                                        tradesh.write(dst_path_def+'\n')
                                        tradesh.write(srcparent_path+'\n')
                                        tradesh.write(dstparent_path+'\n')
                                        tradesh.write(echosh+'\n')
                                        tradesh.write(checkanddistcp_stmts+'\n')
                                        tradesh.write(done+'\n')
                                        tradesh.write("\n")
                                        tradesh.write("#################################################\n")
                                elif matchDTB.group(3) == "supchain":
                                    if dstdb == "leco":
                                        lecosupchainsh.write(var_src_db+'\n')
                                        lecosupchainsh.write(var_src_tb+'\n')
                                        lecosupchainsh.write(var_dst_db+'\n')
                                        lecosupchainsh.write(var_dst_tb+'\n')
                                        lecosupchainsh.write(dtsh+'\n')
                                        lecosupchainsh.write(country_codesh+'\n')
                                        lecosupchainsh.write(ccsh+'\n')
                                        lecosupchainsh.write(src_path_def+'\n')
                                        lecosupchainsh.write(dst_path_def+'\n')
                                        lecosupchainsh.write(srcparent_path+'\n')
                                        lecosupchainsh.write(dstparent_path+'\n')
                                        lecosupchainsh.write(echosh+'\n')
                                        lecosupchainsh.write(checkanddistcp_stmts+'\n')
                                        lecosupchainsh.write(done+'\n')
                                        lecosupchainsh.write("\n")
                                        lecosupchainsh.write("#################################################\n")
                                    else:
                                        supchainsh.write(var_src_db+'\n')
                                        supchainsh.write(var_src_tb+'\n')
                                        supchainsh.write(var_dst_db+'\n')
                                        supchainsh.write(var_dst_tb+'\n')
                                        supchainsh.write(dtsh+'\n')
                                        supchainsh.write(country_codesh+'\n')
                                        supchainsh.write(ccsh+'\n')
                                        supchainsh.write(src_path_def+'\n')
                                        supchainsh.write(dst_path_def+'\n')
                                        supchainsh.write(srcparent_path+'\n')
                                        supchainsh.write(dstparent_path+'\n')
                                        supchainsh.write(echosh+'\n')
                                        supchainsh.write(checkanddistcp_stmts+'\n')
                                        supchainsh.write(done+'\n')
                                        supchainsh.write("\n")
                                        supchainsh.write("#################################################\n")
                                else:
                                    if dstdb == "leco":
                                        lecooutsh.write(var_src_db+'\n')
                                        lecooutsh.write(var_src_tb+'\n')
                                        lecooutsh.write(var_dst_db+'\n')
                                        lecooutsh.write(var_dst_tb+'\n')
                                        lecooutsh.write(dtsh+'\n')
                                        lecooutsh.write(country_codesh+'\n')
                                        lecooutsh.write(ccsh+'\n')
                                        lecooutsh.write(src_path_def+'\n')
                                        lecooutsh.write(dst_path_def+'\n')
                                        lecooutsh.write(srcparent_path+'\n')
                                        lecooutsh.write(dstparent_path+'\n')
                                        lecooutsh.write(echosh+'\n')
                                        lecooutsh.write(checkanddistcp_stmts+'\n')
                                        lecooutsh.write(done+'\n')
                                        lecooutsh.write("\n")
                                        lecooutsh.write("#################################################\n")
                                    else:
                                        outsh.write(var_src_db+'\n')
                                        outsh.write(var_src_tb+'\n')
                                        outsh.write(var_dst_db+'\n')
                                        outsh.write(var_dst_tb+'\n')
                                        outsh.write(dtsh+'\n')
                                        outsh.write(country_codesh+'\n')
                                        outsh.write(ccsh+'\n')
                                        outsh.write(src_path_def+'\n')
                                        outsh.write(dst_path_def+'\n')
                                        outsh.write(srcparent_path+'\n')
                                        outsh.write(dstparent_path+'\n')
                                        outsh.write(echosh+'\n')
                                        outsh.write(checkanddistcp_stmts+'\n')
                                        outsh.write(done+'\n')
                                        outsh.write("\n")
                                        outsh.write("#################################################\n")
            outsh.close()
            loansh.close()
            finsh.close()
            paysh.close()
            risksh.close()
            accountingsh.close()
            assetssh.close()
            tradesh.close()

            lecooutsh.close()
            lecoloansh.close()
            lecofinsh.close()
            lecopaysh.close()
            lecorisksh.close()
            lecoaccountingsh.close()
            lecoassetssh.close()
            lecotradesh.close()
            ifile.close()

    hive_client.close()

    dir = "./migration_dir"
    remove_empty_file(dir)

if __name__ == '__main__':
    main()
