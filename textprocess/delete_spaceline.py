#!/usr/bin/python
# -*- coding=utf-8 -*-

import os
import re

inf = open("./create_tables.sql","r")
ouf = open("./create_tables_withoutspaceline.sql","w")

def del_spaceline():
    while True:
        line = inf.readline()
        if not line:
            break
        if len(line.strip()) != 0:
        #match_space=re.search(r'"\n"',str(strip_line))
        #if not match_space:
            ouf.write(line)

if __name__ == "__main__":
    del_spaceline()
