#!/usr/bin/python
#-*- encoding=utf-8 -*-
import sys
import json
import re
from Queue import Queue

reload(sys)
sys.setdefaultencoding("utf-8")

def bfs_parse_json(obj,key):
    root_key = key
    collect = list()
    key_queue = Queue()
    val_queue = Queue()
    key_queue.put(root_key)
    val_queue.put(obj)
    while key_queue.empty() == False:
        to_process_k = key_queue.get()
        to_process_v = val_queue.get()
        if isinstance(to_process_v,dict):
            for k,v in to_process_v.items():
                key_queue.put(to_process_k+"."+k)
                val_queue.put(v)
        elif isinstance(to_process_v,list):
            for index, item in enumerate(to_process_v):
                key_queue.put(to_process_k+"["+str(index)+"]")
                val_queue.put(item)
        else:
            collect.append({to_process_k:to_process_v})
    return collect

def delete_bracket(keystr):
    matchRes = re.match(r'(.*)\[\d+\](.*)', keystr, re.M|re.I)
    if matchRes:
        if matchRes.group(1) is not None:
            newKey = matchRes.group(1)
            if matchRes.group(2) is not None:
                newKey = matchRes.group(1)+matchRes.group(2)
        return delete_bracket(newKey)
    else:
        return keystr

def bfs_parse_json_by_level(obj,key,target,level):
    root_key = key
    collect = list()
    key_queue = Queue()
    val_queue = Queue()
    key_queue.put(root_key)
    val_queue.put(obj)
    while key_queue.empty() == False:
        to_process_k = key_queue.get()
        to_process_v = val_queue.get()
        if isinstance(to_process_v,dict):
            for k,v in to_process_v.items():
                key_queue.put(to_process_k+"."+k)
                val_queue.put(v)
        elif isinstance(to_process_v,list):
            for index, item in enumerate(to_process_v):
                key_queue.put(to_process_k+"["+str(index)+"]")
                val_queue.put(item)
        else:
            collect.append({to_process_k:to_process_v})
    return collect

if __name__ == "__main__":
    json_file = open(str(sys.argv[1]), "r")
    try:
        while True:
            line=json_file.readline().strip('\n')
            if not line:
                break
            jsonobj = json.loads(line)
            result = bfs_parse_json(jsonobj,'')
            print result
    except Exception as err:
        print (err)
    finally:
        pass
