#!/usr/bin/python
#-*- encoding=utf-8 -*-
import sys
import json
import re
from Queue import Queue

reload(sys)
sys.setdefaultencoding("utf-8")

def recur_parse_json(obj,key):
    return read(obj,key)

def read(obj,key):
    collect = list()
    for k in obj:
        v = obj[k]
        if isinstance(v,dict):
            collect.extend(read(v,k))
        elif isinstance(v,list):
            if key=='':
                collect.extend(read_list(v,k))
            else:
                collect.extend(read_list(v,key+"."+k))
        else:
            if key=='':
                collect.append({k:v})
            else:
                collect.append({str(key)+"."+k:v})
    return collect

def read_list(obj,key):
    collect = list()
    for index,item in enumerate(obj):
        #print "index = %s" % index
        #print "item = %s" % item
        if isinstance(item,dict):
            for k in item:
                v = item[k]
                if isinstance(v,dict):
                    collect.extend(read(v,key+"["+str(index)+"]"+"."+k))
                elif isinstance(v,list):
                    collect.extend(read_list(v,key+"["+str(index)+"]"+"."+k))
                else:
                    collect.append({key+"["+str(index)+"]"+"."+k:v})
        else:
            v = item
            if isinstance(v,dict):
                collect.extend(read(v,key+"["+str(index)+"]"))
            elif isinstance(v,list):
                collect.extend(read_list(v,key+"["+str(index)+"]"))
            else:
                collect.append({key+"["+str(index)+"]":v})
    return collect

def delete_bracket(keystr):
    matchRes = re.match(r'(.*)\[\d+\](.*)', keystr, re.M|re.I)
    if matchRes:
        if matchRes.group(1) is not None:
            newKey = matchRes.group(1)
            if matchRes.group(2) is not None:
                newKey = matchRes.group(1)+matchRes.group(2)
        return delete_bracket(newKey)
    else:
        return keystr

if __name__ == "__main__":
    json_file = open(str(sys.argv[1]), "r")
    try:
        while True:
            line=json_file.readline().strip('\n')
            if not line:
                break
            jsonobj = json.loads(line)
            result = recur_parse_json(jsonobj,'')
            print result
    except Exception as err:
        print (err)
    finally:
        pass
