#!/usr/bin/python
#-*- encoding=utf-8 -*-

import sys
import json
import MySQLdb
#from tabulate import tabulate

reload(sys)
sys.setdefaultencoding("utf-8")

json_file = open("./resources/InputFile.json", "r")

def read(obj,key):
	collect = list()
	for k in obj:
		v = obj[k]
		if isinstance(v,dict):
			collect.extend(read(v,k))
		elif isinstance(v,list):
			if key=='':
				collect.extend(read_list(v,k))
			else:
				collect.extend(read_list(v,key+"."+k))
		else:
			if key=='':
				collect.append({k:v})
			else:
				collect.append({str(key)+"."+k:v})
	return collect
	
def read_list(obj,key):
	collect = list()
	for index,item in enumerate(obj):
		#print "index = %s" % index 
		#print "item = %s" % item
		if isinstance(item,dict):
			for k in item:
				v = item[k]
				if isinstance(v,dict):
					collect.extend(read(v,key+"["+str(index)+"]"))
				elif isinstance(v,list):
					collect.extend(read_list(v,key+"["+str(index)+"]"))
				else:
					collect.append({key+"["+str(index)+"]"+"."+k:v})
		else:
			v = item
			if isinstance(v,dict):
				collect.extend(read(v,key+"["+str(index)+"]"))
			elif isinstance(v,list):
				collect.extend(read_list(v,key+"["+str(index)+"]"))
			else:
				collect.append({key+"["+str(index)+"]":v})
	return collect

def create_table(table_name):
	table_schema_file = open("./resources/table_schema.cfg", "r")
	#create_table = "create table if not exists "+table_name+" ("
	create_table = "drop table if exists "+table_name+";create table if not exists "+table_name+" ("
	while True:
		line = table_schema_file.readline()
		if not line:
			break
		create_table = create_table+line.strip()
	create_table = create_table+" ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;"
	return create_table

def get_schema(table_name):
	table_schema_file = open("./resources/table_schema.cfg", "r")
	schema_list = list()
	while True:
		line = table_schema_file.readline()
		if not line:
			break
		schema_list.append(line.strip().split(" ")[0])
	schema = ",".join(schema_list)
	return schema

def get_fields_type(table_name):
	table_schema_file = open("./resources/table_schema.cfg", "r")
	type_list = list()
	while True:
		line = table_schema_file.readline()
		if not line:
			break
		type_list.append(line.strip().split(" ")[1].replace(",",""))
	return type_list 

def get_values(table_name, dict_input):
	values = list()
	fields_type = get_fields_type(table_name)
	for item in dict_input:
		for value in item.values():
			value = str(value).replace('''\"''', '''\\\"''')
			values.append('''"'''+str(value)+'''"''')
			#if str(fields_type[len(values)]).startswith("varchar"):
			#	values.append('''"'''+str(value)+'''"''')
			#else:
			#	values.append(str(value))
	str_values = ",".join(values)
	return str_values 

if __name__ == "__main__":
	try:
		conn = MySQLdb.Connection(host="localhost", user="root", passwd="111111", charset="utf8")
		conn.select_db('test')
		cursor = conn.cursor(MySQLdb.cursors.DictCursor)
		#cursor.execute("SET NAMES utf8")
		create_mysql_table = create_table("json_data")
		cursor.execute(create_mysql_table)
		cursor.close()

		cursor = conn.cursor(MySQLdb.cursors.DictCursor)
		table_schema = get_schema("json_data")
		while True:
			line = json_file.readline()
			if not line:
				break
			jsonobj = json.loads(line)
			result = read(jsonobj,'')
			#print read(jsonobj,'')
			#print tabulate(read(jsonobj,''))

			table_values = get_values("json_data", result)
			insert_sql = "insert into json_data ("+table_schema+")"+" values ("+table_values+")"
			cursor.execute(insert_sql)
		cursor.close()

	finally:
		if conn:
			conn.close()
