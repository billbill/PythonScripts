#!/usr/bin/python
#-*- encoding=utf-8 -*-
import json
import re
from queue import Queue
import subprocess 
import re

file_list = open('/home/tyler/scripts/files/es_non_hive.lst','r')
#file_list = open('/home/tyler/scripts/files/test_es_non_hive.lst','r')

# data types mapping between es and hive, https://www.elastic.co/guide/en/elasticsearch/hadoop/current/hive.html
es_types = ['null','boolean','byte','short','int','long','double','float','text','string','binary','date','map','array','not supported']
hv_types = ['void','boolean','tinyint','smallint','int','bigint','double','float','string','binary','timestamp','struct','map','array','union','decimal','date','varchar','char']
es2hive_types_mapping = {
    'null':'void',
    'boolean':'boolean',
    'byte':'tinyint',
    'short':'smallint',
    'int':'int',
    'long':'bigint',
    'double':'double',
    'float':'float',
    'text':'string',
    'string':'string',
    'binary':'binary',
    'date':'timestamp',
    'map':'map',
    'array':'array'}

def bfs_parse_json(obj,key):
    root_key = key
    collect = list()
    key_queue = Queue()
    val_queue = Queue()
    key_queue.put(root_key)
    val_queue.put(obj)
    while key_queue.empty() == False:
        to_process_k = key_queue.get()
        to_process_v = val_queue.get()
        if isinstance(to_process_v,dict):
            for k,v in to_process_v.items():
                key_queue.put(to_process_k+"."+k)
                val_queue.put(v)
        elif isinstance(to_process_v,list):
            for index, item in enumerate(to_process_v):
                key_queue.put(to_process_k+"["+str(index)+"]")
                val_queue.put(item)
        else:
            collect.append({to_process_k:to_process_v})
    return collect

def delete_bracket(keystr):
    matchRes = re.match(r'(.*)\[\d+\](.*)', keystr, re.M|re.I)
    if matchRes:
        if matchRes.group(1) is not None:
            newKey = matchRes.group(1)
            if matchRes.group(2) is not None:
                newKey = matchRes.group(1)+matchRes.group(2)
        return delete_bracket(newKey)
    else:
        return keystr

def bfs_parse_json_by_level(obj,key,target,level):
    root_key = key
    collect = list()
    key_queue = Queue()
    val_queue = Queue()
    key_queue.put(root_key)
    val_queue.put(obj)
    while key_queue.empty() == False:
        to_process_k = key_queue.get()
        to_process_v = val_queue.get()
        if isinstance(to_process_v,dict):
            for k,v in to_process_v.items():
                key_queue.put(to_process_k+"."+k)
                val_queue.put(v)
        elif isinstance(to_process_v,list):
            for index, item in enumerate(to_process_v):
                key_queue.put(to_process_k+"["+str(index)+"]")
                val_queue.put(item)
        else:
            collect.append({to_process_k:to_process_v})
    return collect

def gen_hive_schema_from_es(index_name,cmd):
    (status,jsonstr) = subprocess.getstatusoutput(cmd)
    if status==0:
        print("######### Get es's index struct command execute successful! #########")
        jsonobj = json.loads(jsonstr)
        result = bfs_parse_json(jsonobj,'')
        key_list_value = []
        struct_value = ''
        key_struct_value = {}
        final_list_value = [] 
        final_dict_value = {} 
        es_key_type = {}
        es_index_type = set()
        for data_and_type in result:
            #print(data_and_type)
            for key,value in data_and_type.items():
                if (value != 'keyword' 
                    and value !='nested'
                    and value !='object'
                    and value != 'integer'
                ):
                    try:
                        value = es2hive_types_mapping.get(str(value))
                    except Exception as err:
                        print(err)
                    type_suffix_match = re.match(r'(.*\.type)',key,re.I)
                    if type_suffix_match:
                        key = type_suffix_match.group(0)
                        filter_match = re.match(r'.*fields\.keyword\.type',key,re.I)
                        if filter_match:
                            pass
                        else:
                            properties_match = re.findall(r'properties',key)
                            if(len(properties_match)) > 1:
                                # fixme, there maybe array type
                                key_match = re.match(r'(.*)(\.mappings\.)(\w+)(\.properties\.)(\w+)(\.properties\.)(\w+)(\..*)',key,re.I)
                                if key_match:
                                    key_level_0 = key_match.group(5)
                                    key_level_1 = key_match.group(7)
                                    struct_value = struct_value+key_level_1+":"+value+','
                                    key_struct_value.setdefault(key_level_0,'')
                                    key_struct_value[key_level_0] = struct_value
                                    es_key_type.setdefault(key_level_0,'')
                                    es_key_type[key_level_0] = key_match.group(3)
                            else:
                                key_match = re.match(r'(.*)(\.mappings\.)(\w+)(\.properties\.)(\w+)(\.).*',key,re.I)
                                if key_match:
                                    key = key_match.group(5)
                                    key_list_value.append(key+":"+value)
                                    es_key_type.setdefault(key,'')
                                    es_key_type[key] = key_match.group(3)
                                pass
                else:
                    pass
        for item in key_list_value:
            final_list_value.append(item)
        for key,value in key_struct_value.items():
            final_list_value.append(key+":"+"struct<"+value[:-1]+">")
        for item in final_list_value:
            key_match = re.match(r'(\w+):(.*)',item,re.I)
            key = key_match.group(1)
            value = key_match.group(2)
            type_value = es_key_type.get(key) 
            query_cmd = '{"query":{"multi_match":{"query":"*","fields":["'+key+'"]}}}'
            cmd = 'curl -XGET http://10.102.2.189:7536/'+index_name+'/'+type_value+'/_search '+query_cmd+' 2>/dev/null' # this is important
            (status,result) = subprocess.getstatusoutput(cmd)
            array_match = re.match(r'.*\"(\w+)\"\:\[\{.*',result,re.I)
            if array_match:
                if key == array_match.group(1):
                    schema_key = array_match.group(1)
                    schema_type = "array<"+key_match.group(2)+">"
                    #print(key+":"+schema_type)
                    final_dict_value.setdefault(schema_key,'')
                    final_dict_value[schema_key] = schema_type
                else:
                    schema_match = re.match(r'(\w+):(.*)',item,re.I)
                    schema_key = schema_match.group(1)
                    schema_type = schema_match.group(2)
                    if schema_match:
                        final_dict_value.setdefault(schema_key,'')
                        final_dict_value[schema_key] = schema_type
                    else:
                        pass
            else:
                schema_match = re.match(r'(\w+):(.*)',item,re.I)
                schema_key = schema_match.group(1)
                schema_type = schema_match.group(2)
                if schema_match:
                    final_dict_value.setdefault(schema_key,'')
                    final_dict_value[schema_key] = schema_type
                else:
                    pass
            #print(key+":"+value)
            #print(es_key_type.get(key))
        for key,value in es_key_type.items():
            es_index_type.add(value)
        #print("-------------------------------")
    else:
        print(cmd+'execute error!\n')

    return es_index_type,final_dict_value

def gen_hql_file_from_es(es_host,es_port,index_name,out_dir,debug_flag):
    try:
        if(debug_flag==True):
            print("######### Index: "+index_name+" #########")
        #cmd = 'curl -XGET http://10.102.2.189:7536/'+index_name+'/_mapping?pretty'
        cmd = 'curl -XGET http://10.102.2.189:7536/'+index_name+'/_mapping?pretty 2>/dev/null' # this is important
        es_index_type,hive_schema = gen_hive_schema_from_es(index_name,cmd)
        for item in es_index_type: # es_index_type's size is always 1
            if(debug_flag==True):
                print(index_name+"/"+item)
        #for key,value in hive_schema.items():
        #    if(debug_flag==True):
        #        print(key+":"+value)
        es_resource = str(index_name)+"/"+str(es_index_type.pop()) # es_index_type's size is always 1
        str_hive_schema = ''
        str_es_mapping = ''
        for key,value in hive_schema.items():
            str_hive_schema = str_hive_schema+','+str(key).lower()+' '+str(value)+'\n'
            str_es_mapping = str_es_mapping+key.lower()+":"+key+','
        format_str_hive_schema = str_hive_schema[1:-1]
        format_str_es_mapping = str_es_mapping[:-1]

        if(debug_flag==True):
            print("hive schema is:")
            print(format_str_hive_schema)
        if(debug_flag==True):
            print("es mapping is:")
            print(format_str_es_mapping)

        sql_file = 'stg_sayabc_es_' + str(index_name)
        out_sql_file = open(str(out_dir)+'/'+str(sql_file)+'.sql','w')
        out_sql_file.write("use stg;\n")
        out_sql_file.write("\n")
        out_sql_file.write("-- drop table if exists "+str(sql_file)+";\n")
        out_sql_file.write("create external table if not exists "+str(sql_file)+"("+"\n")
        out_sql_file.write(format_str_hive_schema)
        out_sql_file.write("\n")
        out_sql_file.write(")\n")
        out_sql_file.write("STORED BY 'org.elasticsearch.hadoop.hive.EsStorageHandler'\n")
        out_sql_file.write("TBLPROPERTIES (\n")
        out_sql_file.write("'COLUMN_STATS_ACCURATE'='false',\n")
        out_sql_file.write("'es.index.auto.create'='true',\n")
        out_sql_file.write("'es.mapping.names'='"+format_str_es_mapping+"',\n")
        out_sql_file.write("'es.nodes'='"+str(es_host)+"',\n")
        out_sql_file.write("'es.nodes.wan.only'='true',\n")
        out_sql_file.write("'es.port'='"+str(es_port)+"',\n")
        out_sql_file.write("'es.read.metadata'='false',\n")
        out_sql_file.write("'es.resource'='"+str(es_resource)+"'\n")
        out_sql_file.write(");\n")
        out_sql_file.close()
    except Exception as err:
        print(err)
    finally:
        pass

if __name__ == "__main__":
    es_host = '10.102.2.189,10.102.2.190,10.102.12.87'
    es_port = '7536'
    out_dir = '/home/tyler/data/dw/stg/sql'
    try:
        while True:
            line = file_list.readline()
            index_name = line.strip()
            if not index_name:
                break
            gen_hql_file_from_es(es_host,es_port,index_name,out_dir,debug_flag=True)
    except Exception as err:
        print(err)
    finally:
        pass
    file_list.close()
