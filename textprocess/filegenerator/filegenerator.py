#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import re
import os
reload(sys)
sys.setdefaultencoding("utf-8")

def gen_file(indir,outdir,template_file_fullpath):
    '''
    open the base directory
    '''
    try:
        for dirpath,dirnames,filenames in os.walk(indir):
            #for dirname in dirnames:
            #    print dirname
            for filename in filenames:
                #fullpath=os.path.join(dirpath,filename)
                #print filename
                matchObj = re.match(r'(.*zxt.*)\.sql',filename,re.M|re.I)
                if matchObj:
                    try:
                        if not os.path.exists(outdir):
                            os.makedirs(outdir)
                        outfile = open(outdir+"/"+str(matchObj.group(1))+".sh","w")
                        if os.path.isfile(template_file_fullpath):
                            infile = open(template_file_fullpath,"r")
                            while True:
                                line = infile.readline() # strip() cann't be used in this line, if put it in htis line, the read operation will break ahead when the line is space line but not the end of the file
                                if not line:
                                    break
                                matchFilename = re.match(r'(.*dst_tb_name=")(.*)(")',str(line.strip('\n')),re.M|re.I) or re.match(r'(job=\$basepath\/sql\/)(.*)(\.sql)',str(line.strip('\n')),re.M|re.I)
                                if matchFilename:
                                    outfile.write(matchFilename.group(1)+filename[0:-4]+matchFilename.group(3)+"\n")
                                else:
                                    outfile.write(line)
                        filepath = outdir+"/"+str(matchObj.group(1))+".sh"
                        add_permition('u','x',filepath)
                    except Exception,e:
                        print Exception,":",e
                else:
                    pass
    except Exception,e:
        print Exception,":",e

def add_permition(ug_attr,perm_type,file):
    '''
    exmple add_permition(u,w) 
    exmple add_permition(g,x) 
    exmple add_permition(a,r) 
    '''
    os.system('chmod '+str(ug_attr)+'+'+str(perm_type)+' '+str(file))

if __name__ == "__main__":
    indir="/app/share/hbetl/APP/STG/sql"
    outdir="/app/share/hbetl/APP/STG/bin"
    #outdir=str(os.path.dirname(os.path.realpath(__file__)))+"/testoutdir"
    templatedir=str(os.path.dirname(os.path.realpath(__file__)))+"/templatedir"
    template_file=str(templatedir)+"/xloan_allin_third_jxl_trip_info_stg.sh"
    print "outdir: %s" % outdir
    print "template_file: %s" % template_file
    gen_file(indir,outdir,template_file)
