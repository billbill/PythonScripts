#!/usr/bin/env python
#-*- coding: utf-8 -*-
'''
python3 version
'''

import os
import re
import sys
import xlrd
import csv
from datashape import unicode
#import pandas as pd
import codecs
reload(sys)
sys.setdefaultencoding("utf-8")

# way 1
def csv_from_excel(excel_file):
    workbook = xlrd.open_workbook(excel_file)
    all_worksheets = workbook.sheet_names()
    for worksheet_name in all_worksheets:
        worksheet = workbook.sheet_by_name(worksheet_name)
        your_csv_file = open(''.join([worksheet_name, '.csv']), 'wb')
        wr = csv.writer(your_csv_file, quoting=csv.QUOTE_ALL)

        for rownum in range(worksheet.nrows):
            wr.writerow([unicode(entry).encode("utf-8") for entry in worksheet.row_values(rownum)])
        your_csv_file.close()

# way 2
def read_excel(excel_file,csv_file):
    workbook = xlrd.open_workbook(excel_file)
    # print sheet1.name,sheet1.nrows,sheet1.ncols
    sheet1 = workbook.sheet_by_index(0)

    #csvfile = open(csv_file, 'wb')
    #csvfile.write(codecs.BOM_UTF8)
    csvfile = open(csv_file, 'w')
    writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL)

    wtmpfile = open("/tmp/csvtmpfile.%s.csv"%os.getpid(),"w")
    wtmp_writer = csv.writer(wtmpfile, quoting=csv.QUOTE_ALL)

    for row in range(0, sheet1.nrows):
        rows = sheet1.row_values(row)

        def _tostr(cell):
            if type(u'') == type(cell):
                return "\"%s\"" % cell.encode('utf8')
            else:
                return "\"%s\"" % str(cell)

        #print(','.join([_tostr(cell) for cell in rows]))
        #writer.writerow([','.join([_tostr(cell) for cell in rows]).decode("utf-8")])
        #print(','.join([_tostr(cell.strip("\s+")) for cell in rows]).decode("utf-8"))
        wtmp_writer.writerow([','.join([_tostr(cell).replace(","," ") for cell in rows]).decode("utf-8")])

    keyfile = open("/tmp/keyfile.%s.csv"%os.getpid(),"w")
    keywriter = csv.writer(keyfile, quoting=csv.QUOTE_ALL)

    with open("/tmp/csvtmpfile.%s.csv"%os.getpid(),"r") as rtmpfile:
        reader = csv.reader(rtmpfile)
        result = {}
        for row in reader:
            def _del_line_break(src_str):
                line = ''
                for entry in strrow[11:].splitlines():
                    line = line + entry.strip("\n")
                return line

            def _split_orgname_orgcode(src_str):
                matchT = re.match(r'(.*)\((\d+)\)(.*)',src_str,re.IGNORECASE)
                if matchT:
                    if matchT.group(2)=='Canada':
                        print(matchT.group(1))
                    line = matchT.group(1)+','+matchT.group(2)+matchT.group(3)
                else:
                    line = src_str
                return line

            strrow = ''.join(row).decode("utf-8")
            keystr = strrow[1:9]
            matchK = re.match(r'(line\d{4})',keystr)
            if matchK:
                result.setdefault(matchK.group(1),_split_orgname_orgcode(_del_line_break(strrow[11:]).replace("\"","")))

        for key in sorted(result.keys()):
            #print(key)
            #print(result.get(key))
            #keywriter.writerow(result.get(key))
            keyfile.write(result.get(key)+"\n")
    rtmpfile.close()
    keyfile.close()

    csvfile.close()

## way 3
#def excel_to_csv(excel_file,csv_file):
#    data_xls = pd.read_excel(excel_file, 'sheet1', index_col=None)
#    data_xls.to_csv(csv_file, encoding='utf-8')

if __name__ == "__main__":
    excel_file = sys.argv[1]
    csv_file = sys.argv[2]
    #way 1
    #csv_from_excel(excel_file)
    #way 2
    read_excel(excel_file,csv_file) #use python 2.7 finally because of the coding type
    #way 3
    #excel_to_csv(excel_file,csv_file)
